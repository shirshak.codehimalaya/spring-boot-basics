package com.basiccrud.demo.endpoints;

public class Endpoints {

    public static final String REGISTER = "/register";
    public static final String USERS = "/users";
    public static final String DELETE= "/users/delete";
    public static final String UPDATE = "/users/update";
    public static final String CREATEROLE  = "/createRole";
    public static final String ALLROLES = "/allRoles";
    public static final String ROLES= "/roles";
    public static final String EMPLOYEES = "/employees";
    public static final String USERBYROLES = "/getRoles";
}
