package com.basiccrud.demo.entity;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "roles_table")
public class Roles {

    @Id
  @GeneratedValue( strategy = GenerationType.AUTO)
    private long roleId;

    @Column(name = "role_name")
    private String roleName;

    @ManyToMany(mappedBy = "roles")
    private List<User> users;

    public Roles(){

    }
   public Roles(long roleId,String roleName){
        this.roleName = roleName;
        this.roleId = roleId;
   }

    public Roles(long roleId, String roleName, List<User> users) {
        this.roleId = roleId;
        this.roleName = roleName;
        this.users = users;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
