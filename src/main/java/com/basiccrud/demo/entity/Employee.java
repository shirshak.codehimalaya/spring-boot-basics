package com.basiccrud.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long employeeId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email_address")
    private String empEmail;

   /* @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department; */


    public Employee(){

    }

  /*  public Employee(long employeeId, String firstName, String lastName, String empEmail, Department department) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.empEmail = empEmail;
        this.department = department;
    }

    public void setDepartment(Department dep){
        this.department = dep;
    }
    public Department getDepartment(){

        return department;
    } */

    public Employee(long employeeId, String firstName, String lastName, String empEmail) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.empEmail = empEmail;

    }
    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmpEmail() {
        return empEmail;
    }

    public void setEmpEmail(String empEmail) {
        this.empEmail = empEmail;
    }
}
