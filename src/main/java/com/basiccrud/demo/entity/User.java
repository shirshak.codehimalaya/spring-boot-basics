package com.basiccrud.demo.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import org.hibernate.validator.constraints.Length;


import java.util.List;


@Entity
@Table(name = "user_table")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long userId;

    @Column(name = "user_name")
    @NotBlank(message = "{api.response.username.mandatory}")
    @Size(min = 2,max = 25,message = "{api.response.username.size.limit}")
    private String username;
    @Column(name = "user_password")
    @NotBlank(message ="{api.response.password.mandatory}")
  //  @Size(min = 2,max = 25,message = "{api.response.password.size.limit}")
  //  @Length(min = 2, max = 30 , message = "{api.response.password.size.limit}")
    private String password;
    @Column(name = "user_email")
    @Email(message ="{api.response.invalid.email}")
    private String email;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
    joinColumns = @JoinColumn(name = "user_id"),
    inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Roles> roles;

    public User(){

    }

    public User(long userId, String username, String password, String email, List<Roles> roles) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.email = email;
        this.roles = roles;
    }

    public List<Roles> getRoles() {
        return roles;
    }

    public void setRoles(List<Roles> roles) {
        this.roles = roles;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
