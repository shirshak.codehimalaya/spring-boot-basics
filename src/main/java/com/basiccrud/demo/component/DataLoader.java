package com.basiccrud.demo.component;

import com.basiccrud.demo.entity.Roles;
import com.basiccrud.demo.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

//this component automatically populates the roles table when application is just started.
@Component
public class DataLoader implements ApplicationRunner {

    private RolesService rolesService;

    @Autowired
    public DataLoader(RolesService rolesService){
        this.rolesService = rolesService;
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
          if(rolesService.getCount() == 0){
              Roles roleAdmin = new Roles(1,"ROLE_ADMIN");
              Roles roleUser = new Roles(2,"ROLE_USER");

                 rolesService.createRole(roleAdmin);
                 rolesService.createRole(roleUser);
          }
    }
}
