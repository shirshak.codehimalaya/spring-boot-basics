package com.basiccrud.demo.component;

import com.basiccrud.demo.dto.UserDTO;
import com.basiccrud.demo.entity.User;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.util.ContentCachingRequestWrapper;

import java.io.BufferedInputStream;
import java.net.http.HttpClient;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

@Component
public class RequestInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String payload = new String(request.getInputStream().readAllBytes(), request.getCharacterEncoding());
        //String payload = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
       /* request.setAttribute("payload", payload);
        JSONObject jsonObject = new JSONObject(payload);
        jsonObject.put("username", "shirshak");
        String modifiedResponse = jsonObject.toString(); */
        User user  = null;

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(payload);
        if (jsonNode.isObject()) {
            ((ObjectNode) jsonNode).put("username", "shirshak");
           user = objectMapper.convertValue(jsonNode, User.class);
           // payload = objectMapper.writeValueAsString(jsonNode);
        }


        request.setAttribute("payload", user);

        //System.out.println(request.getRequestURI());
        return true;

    }

 /*  private String modifyRequestBody(String requestBody){
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(payload);
        ((ObjectNode) jsonNode).put("username", "shirshak");
        payload = objectMapper.writeValueAsString(jsonNode);
        request.setAttribute("payload", payload);
    } */
}