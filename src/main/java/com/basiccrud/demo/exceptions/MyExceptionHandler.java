package com.basiccrud.demo.exceptions;

import com.basiccrud.demo.pojo.ExceptionMessage;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class MyExceptionHandler {


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
      public ExceptionMessage handleInvalidArgument(MethodArgumentNotValidException ex){
           Map<String,String> errorMap = new HashMap<>();
           ex.getBindingResult().getFieldErrors()
                   .forEach( error -> errorMap.put(error.getField(),error.getDefaultMessage()));

           return new ExceptionMessage(errorMap);
      }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public ExceptionMessage handleEntityNotFoundException(EntityNotFoundException ex){
        return new ExceptionMessage(ex.getMessage());
    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    public ExceptionMessage handleResourceNotFoundException(ResourceNotFoundException ex){
        return new ExceptionMessage(ex.getMessage());
    }

    /*@ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<String> handleEntityNotFoundException(EntityNotFoundException ex){

        String errorMessage = ex.getMessage();

        return new ResponseEntity<>(errorMessage,HttpStatus.NOT_FOUND);
    } */

 /*  @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ExceptionMessage handleInvalidArgument(MethodArgumentNotValidException ex){
        return new ExceptionMessage(new Date(),HttpStatus.BAD_REQUEST,ex.getBindingResult()
                .getFieldErrors().get(0).toString());

   } */


}
