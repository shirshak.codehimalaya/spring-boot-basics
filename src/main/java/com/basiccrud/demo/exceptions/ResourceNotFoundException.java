package com.basiccrud.demo.exceptions;

import com.basiccrud.demo.pojo.ExceptionMessage;

public class ResourceNotFoundException extends Exception{
    public ResourceNotFoundException(String message){
         super(message);
     }
}
