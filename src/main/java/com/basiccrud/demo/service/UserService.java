package com.basiccrud.demo.service;

import com.basiccrud.demo.dto.UserDTO;
import com.basiccrud.demo.entity.Roles;
import com.basiccrud.demo.entity.User;
import com.basiccrud.demo.exceptions.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UserService {


    List<User> getAllUser();

    List<UserDTO> getAllUserDTO();
    UserDTO getUserById(long userId) throws ResourceNotFoundException;

    UserDTO registerUser(User user);
    void deleteUser(long userId) throws ResourceNotFoundException;
    User updateUser(long userId,User userDetails) throws ResourceNotFoundException;

    List<UserDTO> getUsersByRoleName(String roleName);

    List<Roles> getUserRoles(long userId);
}
