package com.basiccrud.demo.service;

import com.basiccrud.demo.dto.RolesDTO;
import com.basiccrud.demo.dto.UserDTO;
import com.basiccrud.demo.entity.Roles;
import com.basiccrud.demo.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface RolesService {

    public Roles createRole(Roles role);
    public long getCount();

    public List<RolesDTO> getAllRoles();



    public List<UserDTO> getUserRole(long roleId);



}
