package com.basiccrud.demo.service;

//import com.basiccrud.demo.entity.Department;
import com.basiccrud.demo.dto.EmployeeDTO;
import com.basiccrud.demo.entity.Employee;

import java.util.List;

public interface EmpService {
     Employee createEmployee(Employee emp);
    List<Employee> getEmployees();
    void deleteEmployee(long empID);
    Employee updateEmployee(long empId,Employee employeeDetails);


    EmployeeDTO getEmployeeById(long empID);
}
