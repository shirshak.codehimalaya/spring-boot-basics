package com.basiccrud.demo.serviceimp;

import com.basiccrud.demo.dto.EmployeeDTO;
import com.basiccrud.demo.entity.Employee;
import com.basiccrud.demo.repo.EmployeeRepo;
import com.basiccrud.demo.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImp implements EmpService {


     EmployeeRepo employeeRepo;//use constructor level injection

    @Autowired
    EmployeeServiceImp(EmployeeRepo employeeRepo){
        this.employeeRepo = employeeRepo;
    }

    @Override
    public Employee createEmployee(Employee emp) {
        System.out.println(employeeRepo.save(emp));
        return employeeRepo.save(emp);
    }

    @Override
    public List<Employee> getEmployees() {
        return employeeRepo.findAll();
    }

    @Override
    public void deleteEmployee(long empID) {
         employeeRepo.deleteById(empID);
    }

    @Override
    public Employee updateEmployee(long empId, Employee employeeDetails) {

        Employee employee = employeeRepo.findById(empId).get();
        employee.setFirstName(employeeDetails.getFirstName());
        employee.setLastName(employeeDetails.getLastName());
        employee.setEmpEmail(employeeDetails.getEmpEmail());

        return employeeRepo.save(employee);
    }
    @Override
    public EmployeeDTO getEmployeeById(long empID) {
        Employee emp = employeeRepo.findById(empID).get();
        EmployeeDTO empDTO = new EmployeeDTO();
        empDTO.setFirst_name(emp.getFirstName());
        empDTO.setLast_name(emp.getLastName());

        return empDTO;
    }


}
