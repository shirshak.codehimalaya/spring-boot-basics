package com.basiccrud.demo.serviceimp;

import com.basiccrud.demo.dto.UserDTO;
import com.basiccrud.demo.entity.Roles;
import com.basiccrud.demo.entity.User;
import com.basiccrud.demo.exceptions.ResourceNotFoundException;
import com.basiccrud.demo.repo.UserRepo;
import com.basiccrud.demo.service.RolesService;
import com.basiccrud.demo.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImp implements UserService {

    private PasswordEncoder passwordEncoder;

    private UserRepo repo;

    private RolesService rolesService;


    public UserServiceImp(PasswordEncoder passwordEncoder,UserRepo userRepo, RolesService rolesService){
        this.passwordEncoder = passwordEncoder;
        this.repo = userRepo;
        this.rolesService = rolesService;
    }


    @Override
    public List<User> getAllUser() {
        return repo.findAll();
    }

    @Override
    public List<UserDTO> getAllUserDTO()  {
        List<User> users = repo.findAll();
        List<UserDTO> userDTOS = new ArrayList<>();

        for(User user : users){
            userDTOS.add(new UserDTO(user.getUsername(), user.getEmail(), user.getUserId()));
        }
        return userDTOS;
    }

    @Override
    public UserDTO getUserById(long userId) throws ResourceNotFoundException{
        User user =  repo.findById(userId).orElseThrow(()-> new ResourceNotFoundException("Username not found"));
        UserDTO userDTO = new UserDTO(user.getUsername(), user.getEmail(),userId);
        return userDTO;
    }

    @Override
    public UserDTO registerUser(User user) {
        UserDTO savedUser = null;
        try{
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
            User specificUser  = repo.save(user);
            savedUser = new UserDTO(specificUser.getUsername(),specificUser.getEmail(),specificUser.getUserId());
        }catch(Exception ex){
           ex.printStackTrace();
        }
      return savedUser;
    }

    @Override
    public void deleteUser(long userId) throws ResourceNotFoundException{
       User user  = repo.findById(userId).orElseThrow(()-> new ResourceNotFoundException("No username with that id found"));
       repo.deleteById(userId);
    }

    @Override
    public User updateUser(long userId, User userDetails) throws ResourceNotFoundException {
        User user  = repo.findById(userId).orElseThrow(() ->new ResourceNotFoundException("No Username with that id found"));
        user.setUserId(userId);
        String hashedPassword = passwordEncoder.encode(userDetails.getPassword());
        user.setPassword(hashedPassword);
        user.setUsername(userDetails.getUsername());
        user.setEmail(userDetails.getEmail());
        user.setRoles(userDetails.getRoles());
        return repo.save(user);
    }

    //used stream api to filter the list of users with specific role
    @Override
    public List<UserDTO> getUsersByRoleName(String roleName) {
        List<User> users = repo.findAll();

        List<User> newUsers = users.stream().filter( user -> user.getRoles().get(0).getRoleName()
                .contains(roleName)).collect(Collectors.toList());
        List<UserDTO> userDTO = new ArrayList<>();

        for(User user : newUsers){
            userDTO.add(new UserDTO(user.getUsername(),user.getEmail(),user.getUserId()));
        }
        return  userDTO;
    }

    @Override
    public List<Roles> getUserRoles(long userId) {
                     User user = repo.findById(userId).get();
                     System.out.println(user.getRoles());
                     return user.getRoles();
    }
}
