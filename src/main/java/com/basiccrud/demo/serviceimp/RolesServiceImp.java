package com.basiccrud.demo.serviceimp;

import com.basiccrud.demo.dto.RolesDTO;
import com.basiccrud.demo.dto.UserDTO;
import com.basiccrud.demo.entity.Roles;
import com.basiccrud.demo.entity.User;
import com.basiccrud.demo.repo.RolesRepo;
import com.basiccrud.demo.service.RolesService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class RolesServiceImp implements RolesService {

    private RolesRepo rolesRepo;

    public RolesServiceImp(RolesRepo rolesRepo){
        this.rolesRepo = rolesRepo;
    }
    @Override
    public Roles createRole(Roles role) {

        return rolesRepo.save(role);
    }
    @Override
    public long getCount() {
        return rolesRepo.count();
    }

    @Override
    public List<RolesDTO> getAllRoles() {
        List<Roles> roles = rolesRepo.findAll();

        List<RolesDTO> rolesDTOS = new ArrayList<>();

        for(Roles roles1 : roles){
            rolesDTOS.add(new RolesDTO(roles1.getRoleName(),roles1.getRoleId()));
        }
        return rolesDTOS;
    }



    @Override
    public List<UserDTO> getUserRole(long roleId) {
        Roles roles = rolesRepo.findById(roleId).get();
        List<User> users = roles.getUsers();
         List<UserDTO> userDTOS = new ArrayList<>();

         for(User user : users){
             userDTOS.add(new UserDTO(user.getUsername(),user.getEmail(),user.getUserId()));
         }
         return userDTOS;
    }
}
