package com.basiccrud.demo.controller;

import com.basiccrud.demo.dto.EmployeeDTO;

import com.basiccrud.demo.endpoints.Endpoints;
import com.basiccrud.demo.entity.Employee;

import com.basiccrud.demo.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class EmployeeController {

    private EmpService empService;

    @Autowired
    public EmployeeController(EmpService empService){
        this.empService = empService;
    }

    @GetMapping(Endpoints.EMPLOYEES+"/{empId}")
    @Secured({"ROLE_ADMIN","ROLE_USER"})
    public EmployeeDTO getEmpByDTO(@PathVariable long empId){

        return empService.getEmployeeById(empId);
    }

    @PostMapping(Endpoints.EMPLOYEES)
    @Secured("ROLE_ADMIN")
    public Employee createEmployee(@RequestBody Employee emp){

        return empService.createEmployee(emp);
    }

//used getMapping instead of requestMapping
   @GetMapping(Endpoints.EMPLOYEES)
   @Secured({"ROLE_ADMIN","ROLE_USER"})
    public List<Employee> readEmployees(){
        return empService.getEmployees();
    }

   @DeleteMapping(Endpoints.EMPLOYEES+"/{empId}")
   @Secured("ROLE_ADMIN")
    public void deleteEmployees(@PathVariable(value="empId") Long id){
        empService.deleteEmployee(id);
    }

    @PutMapping(Endpoints.EMPLOYEES+"/{empId}")
    @Secured("ROLE_ADMIN")
    public Employee updateEmp(@PathVariable(value = "empId") Long id, @RequestBody Employee empDetails){
        return empService.updateEmployee(id,empDetails);
    }
}
