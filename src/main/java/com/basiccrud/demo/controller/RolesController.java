package com.basiccrud.demo.controller;

import com.basiccrud.demo.dto.UserDTO;
import com.basiccrud.demo.endpoints.Endpoints;
import com.basiccrud.demo.entity.Roles;
import com.basiccrud.demo.service.RolesService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RolesController {

    private RolesService rolesService;

    public RolesController(RolesService rolesService){
        this.rolesService = rolesService;
    }

    @PostMapping(Endpoints.CREATEROLE)
    @Secured("ROLE_ADMIN")
    public Roles createRole(@RequestBody Roles rolesDetail){

        return rolesService.createRole(rolesDetail);
    }
    @GetMapping(Endpoints.ALLROLES)
    public long getTotalRoles(){
        return rolesService.getCount();
    }
    @GetMapping(Endpoints.ROLES+"/{roleId}")
    public List<UserDTO> getUserByRoleId(@PathVariable long roleId){

        return rolesService.getUserRole(roleId);
    }
}
