package com.basiccrud.demo.controller;

import com.basiccrud.demo.dto.UserDTO;
import com.basiccrud.demo.endpoints.Endpoints;
import com.basiccrud.demo.entity.User;
import com.basiccrud.demo.exceptions.ResourceNotFoundException;
import com.basiccrud.demo.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
public class UserController {

    private UserService userService;

    @Autowired
   public UserController(UserService userService){
        this.userService=userService;
    }

    @PostMapping(Endpoints.REGISTER)
    public ResponseEntity<?> registerUser(@RequestAttribute("payload") User userDetails){
       /* System.out.println(userDetails);

        return ResponseEntity.ok("ok"); */
        System.out.println(userDetails.getUsername()+" "+userDetails.getEmail()+" "+userDetails.getPassword()
                +" "+userDetails.getUserId());
         return ResponseEntity.ok(userService.registerUser(userDetails));

       // return ResponseEntity.ok("ok");
    }

    @GetMapping(Endpoints.USERS)
    public ResponseEntity<List<UserDTO>> getAllUserDTO(){

        return ResponseEntity.ok(userService.getAllUserDTO());
    }

    @GetMapping(Endpoints.USERS+"/{userId}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable long userId) throws ResourceNotFoundException {

        return ResponseEntity.ok(userService.getUserById(userId));
    }
    @GetMapping(Endpoints.USERBYROLES+"/{roleName}")
    public ResponseEntity<List<UserDTO>> getUserRoleByName(@PathVariable String roleName){

        return ResponseEntity.ok(userService.getUsersByRoleName(roleName));
    }

    @DeleteMapping(Endpoints.DELETE+"/{userId}")
    @Secured("ROLE_ADMIN")
    public void deleteUser(@PathVariable long userId) throws ResourceNotFoundException{
        userService.deleteUser(userId);
    }

    @PutMapping(Endpoints.UPDATE+"/{userId}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> updateUser(@PathVariable long userId,  @RequestBody User userDetails)
            throws ResourceNotFoundException{
        userService.updateUser(userId,userDetails);
        return ResponseEntity.ok("User Updated Successfully");
    }

}
