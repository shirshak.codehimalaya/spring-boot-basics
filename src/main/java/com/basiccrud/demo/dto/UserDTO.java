package com.basiccrud.demo.dto;

public class UserDTO {

    private long userId;
    private String userName;
    private String email;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public UserDTO(String userName, String email, long userId){
        this.userName= userName;
        this.userId = userId;
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
