package com.basiccrud.demo.repo;

import com.basiccrud.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepo extends JpaRepository<User,Long> {

   /* @Query("select u from User u where u.username =:userName")
    User findByUsername(String userName); */

}
