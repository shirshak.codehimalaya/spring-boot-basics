package com.basiccrud.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@EnableWebSecurity
public class MySecurityConfig {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.csrf().disable().authorizeHttpRequests()

                .requestMatchers("/api/employees/{empId}").hasRole("ADMIN")
                .requestMatchers("/api/employees")
                .authenticated()
                .requestMatchers("/users/update/{userId}","/users/delete/{userId}","/createRole","/roles/{roleId}")
                .hasRole("ADMIN")
                .requestMatchers("/users","/users/{userId}","/register","/allRoles").permitAll()
                .anyRequest().permitAll()
                .and().httpBasic()
                .and().formLogin();
        return httpSecurity.build();
    }



}
