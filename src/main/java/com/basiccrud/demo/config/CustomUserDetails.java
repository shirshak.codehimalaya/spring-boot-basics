package com.basiccrud.demo.config;

import com.basiccrud.demo.entity.User;
import com.basiccrud.demo.repo.UserRepo;
import com.basiccrud.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class CustomUserDetails implements UserDetailsService {

    private UserService userService;

    @Autowired
   public CustomUserDetails(UserService userService){
        this.userService = userService;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String email =null;
        String password = null;
        User specificUser = null;
        List<GrantedAuthority> authorities = null;
      List<User> user = userService.getAllUser();
      for(int i=0;i<user.size();i++){
           if(user.get(i).getUsername().contains(username)){
            specificUser = user.get(i);
           }
      }
        System.out.println(specificUser.getUsername());
        try{
            email = specificUser.getEmail();
            password = specificUser.getPassword();
            authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(specificUser.getRoles().get(0).getRoleName()));
        }catch(UsernameNotFoundException exp){
            exp.printStackTrace();
        }
        return new org.springframework.security.core.userdetails.User(email,password,authorities);
    }
}
