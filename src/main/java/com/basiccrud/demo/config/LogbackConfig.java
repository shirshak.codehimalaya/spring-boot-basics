package com.basiccrud.demo.config;

import ch.qos.logback.classic.LoggerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogbackConfig {

    public static void configure(){
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.reset();
        Logger logger = loggerContext.getLogger(Logger.ROOT_LOGGER_NAME);


    }

}
